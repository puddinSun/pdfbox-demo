package com.sunn.demo.pdfbox.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sunn.demo.pdfbox.controller.DownloadRequest;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.xhtmlrenderer.layout.SharedContext;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;

@UtilityClass
@Slf4j
public class PdfConverter {

  @SneakyThrows
  public Resource convertFromHtml(String htmlPath, DownloadRequest request) {

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      Document document = toJsoupDocument(getTemplate());

      Document updatedDocument = replaceElements(document, request);

      ITextRenderer renderer = new ITextRenderer();
      SharedContext sharedContext = renderer.getSharedContext();
      sharedContext.setPrint(true);
      sharedContext.setInteractive(false);
      renderer.setDocumentFromString(updatedDocument.html());
      renderer.layout();
      renderer.createPDF(outputStream);

      // convert to output stream
//      outputStream.write(FileUtils.readFileToByteArray(updatedDocument.));
      return new ByteArrayResource(outputStream.toByteArray());

    } catch (IOException ex) {
      ex.printStackTrace();
      throw new IllegalArgumentException("failed");
    }
  }

  private Document replaceElements(Document originalDocument, DownloadRequest request) {
    Elements elements = originalDocument.getElementsByClass("paragraph__value");

    JsonElement jsonElement = new Gson().toJsonTree(request);
    JsonObject object = (JsonObject) jsonElement;

    for (Element element : elements) {
      String textFormat = format(element.text());
      if (object.get(textFormat) != null) {
        String string = object.get(textFormat).getAsString();
        element.text(element.text().replaceAll(textFormat, string));
        if (element.text().contains("$")) {
          element.text(element.text().replaceAll("\\$", ""));
        }
      } else {
          element.text("-");
      }
    }
    return originalDocument;
  }

  private String format(String text) {
    if (text != null && text.contains("$")) {
      String[] splits = text.split("\\$");

      String s = splits[splits.length - 1];
      if (s.contains("%")) {
        String[] s1 = s.split("%");
        return s1[0];
      }
      return s;
    }
    return text;
  }

  private Document toJsoupDocument(String templateStr) {
    Document document = Jsoup.parse(templateStr, "UTF-8");
    document.outputSettings().syntax(Document.OutputSettings.Syntax.xml);
    return document;
  }

  public String getTemplate() {
    return "<!DOCTYPE html>\n" +
        "<html>\n" +
        "  <head>\n" +
        "    <meta charset=\"utf-8\" />\n" +
        "    <meta name=\"viewport\" content=\"width=device-width\" />\n" +
        "    <title>Loan Offer</title>\n" +
        "    <style>\n" +
        "      body {\n" +
        "        font-family: \"Signika Negative\", sans-serif;\n" +
        "      }\n" +
        "      .mainContent {\n" +
        "        padding: 24px;\n" +
        "        border: 1px solid #c0c6cd;\n" +
        "        border-radius: 6px;\n" +
        "        width: 600px;\n" +
        "        margin: auto;\n" +
        "        background: #f5f9fa;\n" +
        "      }\n" +
        "      .mainTitle {\n" +
        "        font-weight: 700;\n" +
        "        font-size: 18px;\n" +
        "        color: #2f4057;\n" +
        "      }\n" +
        "      .net-amount {\n" +
        "        border-top: 1px dashed #c0c6cd;\n" +
        "        border-bottom: 1px dashed #c0c6cd;\n" +
        "        padding: 10px 0;\n" +
        "      }\n" +
        "      .paragraph {\n" +
        "        margin-bottom: 20px;\n" +
        "        width: 100%;\n" +
        "      }\n" +
        "      .paragraph__label-wrapper {\n" +
        "        display: inline-block;\n" +
        "        width: 49%;\n" +
        "      }\n" +
        "      .paragraph > p {\n" +
        "        text-align: right;\n" +
        "      }\n" +
        "      .paragraph__label {\n" +
        "        font-weight: 400;\n" +
        "        font-size: 14px;\n" +
        "        color: #686868;\n" +
        "      }\n" +
        "      .paragraph__subLabel {\n" +
        "        font-weight: 400;\n" +
        "        font-size: 14px;\n" +
        "        color: #8f8f8f;\n" +
        "      }\n" +
        "      .paragraph__value {\n" +
        "        text-align: right;\n" +
        "        display: inline-block;\n" +
        "        font-weight: 400;\n" +
        "        font-size: 16px;\n" +
        "        color: #464646;\n" +
        "      }\n" +
        "\n" +
        "      .section__title {\n" +
        "        font-weight: 400;\n" +
        "        font-size: 14px;\n" +
        "        color: #686868;\n" +
        "        margin-bottom: 8px;\n" +
        "      }\n" +
        "      .section__subTitle {\n" +
        "        font-weight: 400;\n" +
        "        font-size: 16px;\n" +
        "        color: #464646;\n" +
        "      }\n" +
        "      .section__content {\n" +
        "        padding-left: 20px;\n" +
        "      }\n" +
        "      .condition-section {\n" +
        "        background: #fef2dd;\n" +
        "        border-radius: 4px;\n" +
        "        padding: 14px 20px;\n" +
        "      }\n" +
        "      .condition-list {\n" +
        "        padding-left: 16px;\n" +
        "        margin-bottom: 0;\n" +
        "        list-style-type: lower-alpha;\n" +
        "      }\n" +
        "      .condition-title {\n" +
        "        margin-bottom: 0;\n" +
        "      }\n" +
        "\n" +
        "      .condition-description {\n" +
        "        font-style: normal;\n" +
        "        font-weight: 400;\n" +
        "        font-size: 12px;\n" +
        "        color: #8f8f8f;\n" +
        "      }\n" +
        "      .condition-section > p {\n" +
        "        margin-bottom: 0;\n" +
        "      }\n" +
        "      .paragraph__value-wrapper {\n" +
        "        display: inline-block;\n" +
        "        width: 50%;\n" +
        "        text-align: right;\n" +
        "      }\n" +
        "    </style>\n" +
        "  </head>\n" +
        "  <body>\n" +
        "    <div class=\"mainContent\">\n" +
        "      <h2>Loan Details</h2>\n" +
        "      <div class=\"paragraph\">\n" +
        "        <div class=\"paragraph__label-wrapper\">\n" +
        "          <p class=\"paragraph__label\">Name</p>\n" +
        "        </div>\n" +
        "        <div class=\"paragraph__value-wrapper\">\n" +
        "          <p class=\"paragraph__value\">$name</p>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "      <div class=\"paragraph\">\n" +
        "        <div class=\"paragraph__label-wrapper\">\n" +
        "          <p class=\"paragraph__label\">Account number (if available)</p>\n" +
        "        </div>\n" +
        "\n" +
        "        <div class=\"paragraph__value-wrapper\">\n" +
        "          <p class=\"paragraph__value\">$accountNumber</p>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "\n" +
        "      <div class=\"paragraph\">\n" +
        "        <div class=\"paragraph__label-wrapper\">\n" +
        "          <p class=\"paragraph__label\">Loan amount</p>\n" +
        "        </div>\n" +
        "        <div class=\"paragraph__value-wrapper\">\n" +
        "          <p class=\"paragraph__value\">PHP $loanAmount</p>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "      <div class=\"section\">\n" +
        "        <div class=\"section__titleWrapper\">\n" +
        "          <p class=\"section__title\">Fee(s)</p>\n" +
        "        </div>\n" +
        "        <div class=\"section__content\">\n" +
        "          <div class=\"paragraph\">\n" +
        "            <div class=\"paragraph__label-wrapper\">\n" +
        "              <p class=\"paragraph__label\">Notarial Fee</p>\n" +
        "            </div>\n" +
        "            <div class=\"paragraph__value-wrapper\">\n" +
        "              <p class=\"paragraph__value\">PHP $notarialFee</p>\n" +
        "            </div>\n" +
        "          </div>\n" +
        "        </div>\n" +
        "        <div class=\"section__content\">\n" +
        "          <div class=\"paragraph\">\n" +
        "            <div class=\"paragraph__label-wrapper\">\n" +
        "              <p class=\"paragraph__label\">Doc stamp fee</p>\n" +
        "            </div>\n" +
        "            <div class=\"paragraph__value-wrapper\">\n" +
        "              <p class=\"paragraph__value\">PHP $docStampFee</p>\n" +
        "            </div>\n" +
        "          </div>\n" +
        "        </div>\n" +
        "        <div class=\"section__content\">\n" +
        "          <div class=\"paragraph\">\n" +
        "            <div class=\"paragraph__label-wrapper\">\n" +
        "              <p class=\"paragraph__label\">Processing fee</p>\n" +
        "            </div>\n" +
        "            <div class=\"paragraph__value-wrapper\">\n" +
        "              <p class=\"paragraph__value\">PHP $processingFee</p>\n" +
        "            </div>\n" +
        "          </div>\n" +
        "        </div>\n" +
        "        <div class=\"section__content\">\n" +
        "          <div class=\"paragraph\">\n" +
        "            <div class=\"paragraph__label-wrapper\">\n" +
        "              <p class=\"paragraph__label\">Prepaid interest</p>\n" +
        "            </div>\n" +
        "            <div class=\"paragraph__value-wrapper\">\n" +
        "              <p class=\"paragraph__value\">PHP $prepaidInterest</p>\n" +
        "            </div>\n" +
        "          </div>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "      <div class=\"paragraph net-amount\">\n" +
        "        <div class=\"paragraph__label-wrapper\">\n" +
        "          <p class=\"paragraph__label\">Net disbursement amount</p>\n" +
        "        </div>\n" +
        "        <div class=\"paragraph__value-wrapper\">\n" +
        "          <p class=\"paragraph__value\">PHP $netDisbursementAmount</p>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "      <div class=\"paragraph\">\n" +
        "        <div class=\"paragraph__label-wrapper\">\n" +
        "          <p class=\"paragraph__label\">Disbursement account number</p>\n" +
        "        </div>\n" +
        "        <div class=\"paragraph__value-wrapper\">\n" +
        "          <p class=\"paragraph__value\">$disbursementAccountNumber</p>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "      <div class=\"paragraph\">\n" +
        "        <div class=\"paragraph__label-wrapper\">\n" +
        "          <p class=\"paragraph__label\">Installment amount per term</p>\n" +
        "        </div>\n" +
        "        <div class=\"paragraph__value-wrapper\">\n" +
        "          <p class=\"paragraph__value\">PHP $monthlyRepayment</p>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "      <div class=\"section\">\n" +
        "        <div class=\"section__titleWrapper\">\n" +
        "          <p class=\"section__title\">Loan rate</p>\n" +
        "        </div>\n" +
        "        <div class=\"section__content\">\n" +
        "          <div class=\"paragraph\">\n" +
        "            <div class=\"paragraph__label-wrapper\">\n" +
        "              <p class=\"paragraph__label\">Effective Interest Rate</p>\n" +
        "            </div>\n" +
        "            <div class=\"paragraph__value-wrapper\">\n" +
        "              <p class=\"paragraph__value\">$effectiveInterestRate%</p>\n" +
        "            </div>\n" +
        "          </div>\n" +
        "          <div class=\"paragraph\">\n" +
        "            <div class=\"paragraph__label-wrapper\">\n" +
        "              <p class=\"paragraph__label\">AOR</p>\n" +
        "            </div>\n" +
        "            <div class=\"paragraph__value-wrapper\">\n" +
        "              <p class=\"paragraph__value\">$aor%</p>\n" +
        "            </div>\n" +
        "          </div>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "      <div class=\"paragraph\">\n" +
        "        <div class=\"paragraph__label-wrapper\">\n" +
        "          <p class=\"paragraph__label\">First due date</p>\n" +
        "        </div>\n" +
        "        <div class=\"paragraph__value-wrapper\">\n" +
        "          <p class=\"paragraph__value\">$firstDueDate</p>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "      <div class=\"paragraph\">\n" +
        "        <div class=\"paragraph__label-wrapper\">\n" +
        "          <p class=\"paragraph__label\">Maturity date</p>\n" +
        "        </div>\n" +
        "        <div class=\"paragraph__value-wrapper\">\n" +
        "          <p class=\"paragraph__value\">$maturityDate</p>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "      <div class=\"condition-section\">\n" +
        "        <p>Conditional Charges that may be imposed (if applicable)</p>\n" +
        "        <ol class=\"condition-list\">\n" +
        "          <li>\n" +
        "            <p class=\"condition-title\">Late Charge</p>\n" +
        "            <span class=\"condition-description\"\n" +
        "              >NTB SALAD ETB CSL 3% of Past Due Amount or PHP 500.00, whichever\n" +
        "              is higher, ETB SALAD PHP 125</span\n" +
        "            >\n" +
        "          </li>\n" +
        "          <li>\n" +
        "            <p class=\"condition-title\">Prepayment</p>\n" +
        "            <span class=\"condition-description\">5% of Outstanding Balance</span>\n" +
        "          </li>\n" +
        "          <li>\n" +
        "            <p class=\"condition-title\">\n" +
        "              o/s balance (applicable for re-availments)\n" +
        "            </p>\n" +
        "            <span class=\"condition-description\"\n" +
        "              >Top Up Fee PHP 500 for Loan Amount less than 500,000 PHP 1,000\n" +
        "              for Loan Amount more than or equal to 500,000</span\n" +
        "            >\n" +
        "          </li>\n" +
        "        </ol>\n" +
        "      </div>\n" +
        "    </div>\n" +
        "  </body>\n" +
        "</html>\n";
  }
}
