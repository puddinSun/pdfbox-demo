package com.sunn.demo.pdfbox.controller;

import com.sunn.demo.pdfbox.service.PdfConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.InetAddress;

@Controller
@RequestMapping("/api/v1/file")
@CrossOrigin("*")
@Slf4j
public class FileController {

    @PostMapping("/download")
    public ResponseEntity<Resource> downloadFile(@RequestBody DownloadRequest request) {
        log.info("Request body ==> {}", request);
        Resource resourceOutput = PdfConverter.convertFromHtml("static/template.html", request);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .body(resourceOutput);
    }

    @GetMapping("/metadata")
    public ResponseEntity<FileOutput> getFileMetadata() throws Exception {

        String hostName = InetAddress.getLocalHost().getHostName();
        return ResponseEntity.ok(FileOutput.builder()
                .extension("pdf")
                .fileName("loan-offer-" + hostName)
                .hostName(hostName == null ? "null" : hostName)
                .build());
    }
}
