package com.sunn.demo.pdfbox.controller;

import lombok.Builder;
import lombok.Getter;

@Builder(toBuilder = true)
@Getter
public class FileOutput {
  String fileName;
  String extension;
  String hostName;
}
