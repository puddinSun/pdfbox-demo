package com.sunn.demo.pdfbox.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Builder(toBuilder = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DownloadRequest {

  String name;
  BigDecimal loanAmount;
  String accountNumber;
  String effectiveInterestRate;
  BigDecimal notarialFee;
  BigDecimal docStampFee;
  BigDecimal processingFee;
  Float prepaidInterest;
  BigDecimal netDisbursementAmount;
  String disbursementAccountNumber;
  BigDecimal monthlyRepayment;
  Float aor;
  String firstDueDate;
  String maturityDate;

}
